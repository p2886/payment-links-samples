# Payment Links sample integration

This is a sample integration into Payment Links by Peach Payments.

This sample integration is a service that has an internal list of orders. When an order is created, the service calls Payment Links to generate a payment link for the order.

The service also exposes a webhook endpoint for receiving webhooks from Payment Links, to keep the orders up to date.

## Requirements

- NodeJS
- ngrok (ngrok.com) account
- Peach Payments account with Payment Links activated

## Setup

- `npm install` - Install all required dependencies
- Set required values
  - See `src/config.js` for required environment variables
- `ngrok http 3000` - Setup tunnel for ngrok, this is required for webhooks from Payment Links

## Running

- `npm start` - This will start an ExpressJs service

## Available endpoints

The service exposes a set of endpoints for showing how an integration might work.

`GET http://localhost:3000/` - Loads a list of all payment links created for your merchant.

`GET http://localhost:3000/orders` - Loads a list of "orders" that the service maintains.

`GET http://localhost:3000/orders/<order_id>` - Load a "order" that the service maintains as well as retrieve the payment link status from Pyament Links for that order.

`POST http://localhost:3000/` - Create a new random "order" within the service and generate a payment link for it.

`POST http://localhost:3000/webhook` - An endpoint that receives webhooks from Payment Links in order to update the "order".
