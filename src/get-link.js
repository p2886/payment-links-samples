import axios from "axios";
import { config } from "./config.js";

/**
 * Get the payment link status and details from Payment Links.
 * @param {string} accessToken
 * @param {string} id Payment link Id
 * @returns Payment link details.
 */
export async function getPaymentLinkStatus(accessToken, id) {
  const response = await axios.get(
    `${config.paymentLinksEndpoint}/api/payments/${id}`,
    {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    }
  );

  if (response.status === 200) {
    return response.data;
  }

  throw new Error("Unable to retrieve link");
}
