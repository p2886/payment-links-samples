import express from "express";
import { config } from "./config.js";
import { getAccessToken } from "./get-access-token.js";
import { getLinks } from "./get-links.js";
import { createLink, makePaymentLink } from "./create-link.js";
import { makeId } from "./make-id.js";
import { getPaymentLinkStatus } from "./get-link.js";

const app = express();
app.use(express.json());

const port = 3000;

// Get an access token from Peach Payments' authentication service
// This will allow us to communicate with the Payment Links API as a service.
const accessToken = await getAccessToken(
  config.clientId,
  config.clientSecret,
  config.merchantId
);

// Create a holder for "orders" within our service.
const orders = {};

// get a list of our payment links from Payment Links
app.get("/", async (req, res) => {
  const links = await getLinks(accessToken, config.merchantId);

  return res.json(links);
});

// get a list of our "internal orders"
app.get("/orders", async (req, res) => {
  return res.json(orders);
});

// Get order and status of payment link
app.get("/orders/:id", async (req, res) => {
  const orderId = req.params.id;

  // If the order doesn't exist in our list, let's return a 404.
  if (!(orderId in orders)) {
    return res.status(404).json({ message: "Order not found" });
  }

  // Let's get the status of the payment link from Payment Links
  const paymentLink = await getPaymentLinkStatus(
    accessToken,
    orders[orderId].linkId
  );

  return res.send({
    orderId,
    paid: orders[orderId].paid,
    linkId: orders[orderId].linkId,
    paymentLink,
  });
});

// Create an order and a payment link
app.post("/", async (req, res) => {
  function getOrderId() {
    while (true) {
      const orderId = "ORD" + makeId(12);
      if (!(orderId in orders)) {
        return orderId;
      }
    }
  }

  // Generate a random order id that doesn't currently exist within our system
  const orderId = getOrderId();

  // Generate the payment link details that we need to send to Payment Links.
  const linkDetails = makePaymentLink(orderId, config.publicUrl + "/webhooks");

  try {
    // Attempt to create the actual payment link.
    const link = await createLink(accessToken, config.entityId, linkDetails);

    // Let's store our order and the payment link id together.
    orders[orderId] = {
      linkId: link.id,
      paid: false,
    };

    // Just return the result of the call to Peach Payments.
    return res.json(link);
  } catch (e) {
    return res.status(400).json(e);
  }
});

// Handle webhooks from Payment Links
// We can use this to update our order.
app.post("/webhooks", async (req, res) => {
  const webhook = req.body;
  console.log(webhook);

  switch (webhook.status) {
    // Payment Links sends a webhook with `status` == `completed` when the payment link has been completed by the user.
    case "completed":
      const linkId = webhook.paymentId;

      // Let's find our order based on the payment link id.
      const order = Object.entries(orders).find((item) => {
        return item[1].linkId === linkId;
      });

      // If we have an order for this payment link
      // Let's update the status to paid.
      if (order) {
        orders[order[0]].paid = true;
      }
      break;
    // We will ignore all other Payment Links webhooks.
    default:
      break;
  }

  // We need to return 200 status code when we have handled the webhooks.
  return res.status(200).send();
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
