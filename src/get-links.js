import axios from "axios";
import { config } from "./config.js";

/**
 * Get a list of payment links.
 * @param {string} accessToken
 * @param {string} merchantId
 * @returns A list of payment links
 */
export async function getLinks(accessToken, merchantId) {
  const response = await axios.get(
    `${config.paymentLinksEndpoint}/api/payments?merchant=${merchantId}`,
    {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    }
  );

  if (response.status === 200) {
    return response.data;
  }

  throw new Error("Unable to retrieve links");
}
