import axios from "axios";
import { config } from "./config.js";

const accessTokenCache = {};

/**
 * Get an access token.
 * @param {string} clientId Client Id retrieved from Peach Payment's Dashboard -> Payment Links -> Settings -> Auth.
 * @param {string} clientSecret Client secret retrieved from Peach Payment's Dashboard -> Payment Links -> Settings -> Auth.
 * @param {string} merchantId Merchant Id retrieved from Peach Payment's Dashboard -> Payment Links -> Settings -> Auth.
 * @returns {string} A reusable access token.
 */
export async function getAccessToken(clientId, clientSecret, merchantId) {
  // Let's naively cache the access token
  // We should care about expiry time of the token and flush cache.
  if (clientId in accessTokenCache) {
    return accessTokenCache[clientId];
  }

  // Some basic validation to ensure the function is being called with the required parameters.
  if (!clientId || !clientSecret || !merchantId) {
    throw new Error("Missing config");
  }

  const response = await axios.post(config.authenticationEndpoint, {
    clientId,
    clientSecret,
    merchantId,
  });

  if (response.status === 200) {
    accessTokenCache[clientId] = response.data.access_token;

    return response.data.access_token;
  }

  throw new Error("Unable to authenticate");
}
