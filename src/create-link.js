import axios from "axios";
import { config } from "./config.js";

/**
 * Create a payment link.
 * @param {string} accessToken
 * @param {string} entityId
 * @param {object} linkDetails
 * @returns
 */
export async function createLink(accessToken, entityId, linkDetails) {
  const response = await axios.post(
    `${config.paymentLinksEndpoint}/api/channels/${entityId}/payments`,
    linkDetails,
    {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    }
  );

  if (response.status === 200) {
    return response.data;
  }

  throw new Error("Unable to create link");
}

/**
 * Make a payment links object.
 * @param {string} orderId
 * @param {string} webhookUrl
 * @returns
 */
export function makePaymentLink(orderId, webhookUrl) {
  return {
    payment: {
      merchantInvoiceId: orderId,
      amount: 100,
      currency: "ZAR",
      files: [],
      notes: "",
    },
    customer: {
      givenName: "Grace",
      surname: "Nkosi",
      email: "",
      mobile: "",
      whatsapp: "",
      billing: {
        street1: "",
        city: "",
        state: "",
        postalCode: "",
        country: "",
      },
    },
    options: {
      sendEmail: false,
      sendSms: false,
      sendWhatsapp: false,
      emailCc: "",
      emailBcc: "",
      expiryTime: 5,
      notificationUrl: webhookUrl,
    },
    checkout: {
      tokeniseCard: false,
      forceDefaultMethod: false,
      defaultPaymentMethod: "CARD",
    },
  };
}
