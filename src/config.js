export const config = {
  clientId: process.env.CLIENT_ID,
  clientSecret: process.env.CLIENT_SECRET,
  merchantId: process.env.MERCHANT_ID,

  entityId: process.env.ENTITY_ID,

  publicUrl: process.env.PUBLIC_URL,

  paymentLinksEndpoint:
    process.env.PAYMENTLINKS_ENDPOINT || "https://sandbox-l.ppay.io",
  authenticationEndpoint:
    process.env.AUTHENTICATION_ENDPOINT ||
    "https://sandbox-dashboard.peachpayments.com/api/oauth/token",
};
